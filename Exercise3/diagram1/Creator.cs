﻿namespace Exercise3.diagram1
{
    public class Creator
    {
        public Drivers Drivers { get; }

        public Creator()
        {
            this.Drivers = new Drivers("some text");
        }

        public Some getSome()
        {
            return new Some("some");
        }
    }
}